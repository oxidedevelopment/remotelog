﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace RemoteLog
{
    class Options
    {
        [Option('l', "login", Required = true, HelpText = "The login name.")]
        public String Login { get; set; }

        [Option('p', "password", Required = true, HelpText = "The password.")]
        public String Password { get; set; }

        [Option("ip", Required= true, HelpText = "The IP address of the server to connect to.")]
        public String IP { get; set; }

        public String GetUsage()
        {
            return HelpText.AutoBuild(this);
        }
    }
}
