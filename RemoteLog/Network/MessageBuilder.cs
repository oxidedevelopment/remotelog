﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteLog.Network
{
    class MessageBuilder
    {
        public static byte[] Build(Options options, string content)
        {
            // Format: username:password message

            StringBuilder builder = new StringBuilder();
            builder.Append(options.Login);
            builder.Append(":");
            builder.Append(options.Password);

            builder.Append(" " + content);

            return Encoding.ASCII.GetBytes(builder.ToString());
        }
    }
}
