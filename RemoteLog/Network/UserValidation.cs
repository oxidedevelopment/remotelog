﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace RemoteLog.Network
{
    class UserValidation
    {
        Options options;
        TcpClient client;

        public UserValidation(Options options, TcpClient client)
        {
            this.options = options;
            this.client = client;
        }
        public bool Validate()
        {
            byte[] message = MessageBuilder.Build(options, "validate");
            Messenger.Send(message, client);

            byte[] received = Messenger.Recieve(client, 256);
            String data = Encoding.ASCII.GetString(received);

            if (data.Equals("valid"))
                return true;

            return false;
        }
    }
}
