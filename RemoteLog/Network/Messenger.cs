﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace RemoteLog.Network
{
    class Messenger
    {
        public static void Send(byte[] content, TcpClient client)
        {
            NetworkStream stream = client.GetStream();
            stream.Write(content, 0, content.Length);
        }

        public static byte[] Recieve(TcpClient client, int bufferSize)
        {
            NetworkStream stream = client.GetStream();
            byte[] buffer = new byte[bufferSize];
            stream.Read(buffer, 0, buffer.Length);

            return buffer;
        }
    }
}
