﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Security.Cryptography;
using RemoteLog.Network;


namespace RemoteLog
{
    class Client
    {
        public Options options;

        public Client(Options options)
        {
            this.options = options;
            //Encrypt password right away.
            EncryptPassword();
        }

        private void EncryptPassword()
        {
            byte[] data = Encoding.ASCII.GetBytes(options.Password);
            data = new SHA512Managed().ComputeHash(data);
            options.Password = Encoding.ASCII.GetString(data);
            Console.WriteLine("Password encrypted. " + options.Password);
        }

        public void Start()
        {
            //We will be using TCP.
            TcpClient client = new TcpClient();
            client.Connect(options.IP, 356);

            //Validate the user
            UserValidation validator = new UserValidation(options, client);
            if(!validator.Validate())
            {
                Console.WriteLine("Invalid username or password.");
                Environment.Exit(-1);
            }

            client.GetStream().Close();
            client.Close();
        }
    }
}
