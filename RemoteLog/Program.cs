﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteLog
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                Client client = new Client(options);
                client.Start();
            }
            else
            {
                //Display usage, bad args.
                Console.WriteLine(options.GetUsage());
            }


            #if DEBUG
                Console.ReadKey(true); //So we can read what the app said :)
            #endif
        }
    }
}
