﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RemoteLogServer.Plugins;

namespace RemoteLogServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Check if we have a plugins directory.
            if (!Directory.Exists("plugins"))
            {
                Directory.CreateDirectory("plugins");
            }

            PluginManager.LoadPlugins();

            Server server = new Server();
            server.Start();

            Console.ReadKey(false);
        }
    }
}
