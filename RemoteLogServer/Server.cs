﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using RemoteLogServer.Utils;

namespace RemoteLogServer
{
    class Server
    {
        public void Start()
        {
            // I WANT TO REMOVE THIS MESSY TRY CATCH THING BUT I CAN't HELP ME :((
            TcpListener server = null;
            try
            {
                server = new TcpListener(IPAddress.Parse("127.0.0.1"), 356);
                server.Start();

                byte[] buffer = new byte[4096]; // 4Kb Bit(Pun not intended) much, but you never know.
                string data = String.Empty;

                while (true)
                {
                    Console.WriteLine("Waiting for a connection.");
                    TcpClient client = server.AcceptTcpClient();

                    NetworkStream stream = client.GetStream();
                    int i;

                    //Read data sent.
                    while((i = stream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        //Translate data to a string
                        data = Encoding.ASCII.GetString(buffer, 0, i);
                        Console.WriteLine("Received the following: {0}", data);

                        Parser parser = new Parser();
                        parser.Parse(data, client);
                    }

                    client.Close();
                }

            }
            catch(Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            finally
            {
                server.Stop();
            }
        }
    }
}
