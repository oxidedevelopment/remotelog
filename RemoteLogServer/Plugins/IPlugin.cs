﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace RemoteLogServer.Plugins
{
    interface IPlugin
    {
        string Name { get; }
        string Author { get; }
        void DataReceived(string content, TcpClient client);
    }
}
