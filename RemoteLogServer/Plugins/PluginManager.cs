﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Reflection;

namespace RemoteLogServer.Plugins
{
    class PluginManager
    {
        static List<IPlugin> plugins = new List<IPlugin>();
        public static void LoadPlugins()
        {
            //Load all the plugins so that we spend less time when with a client. 
            string[] plugins = Directory.GetFiles("plugins");
            foreach(string plugin in plugins)
            {
                Assembly.LoadFrom(plugin);
            }

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.GetInterface("IPlugin") != null)
                    {
                        try
                        {
                            IPlugin plugin = Activator.CreateInstance(type) as IPlugin;
                            PluginManager.plugins.Add(plugin);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error while loading plugins.\n" + e.Message);
                        }
                    }
                }
            }
        }

        public static void CallPlugins(string content, TcpClient client)
        {
            foreach(IPlugin plugin in PluginManager.plugins)
            {
                plugin.DataReceived(content, client);
            }
        }
    }
}
