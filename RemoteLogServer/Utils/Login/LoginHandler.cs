﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace RemoteLogServer.Utils.Login
{
    class LoginHandler
    {
        /// <summary>
        /// Save the username password list to a binary format.
        /// </summary>
        public void Save(Logins l)
        {
            BinaryFormatter saverthing = new BinaryFormatter();
            FileStream fs = new FileStream("logins.bin", FileMode.OpenOrCreate);
            try
            {
                saverthing.Serialize(fs, l);
            }
            catch(SerializationException e)
            {
                Console.WriteLine("Failed to save passwords. Quitting.\n" + e.Message);
                //TODO: Create protocol to handle server exceptions
                Environment.Exit(-1);
            }
            finally
            {
                fs.Close();
            }
        }
        /// <summary>
        /// Loads the logins.bin file into a class.
        /// </summary>
        /// <returns></returns>
        public Logins Load()
        {
            Logins logins = null;
            BinaryFormatter loaderthing = new BinaryFormatter();
            FileStream fs = new FileStream("logins.bin", FileMode.Open);
            try
            {
                logins = loaderthing.Deserialize(fs) as Logins; //I feel so cool.
            }
            catch(SerializationException e)
            {
                Console.WriteLine("Failed to load passwords. Quitting.\n" + e.Message);
            }
            finally
            {
                fs.Close();
            }
            return logins;
        }
    }
}
