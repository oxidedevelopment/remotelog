﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteLogServer.Utils.Login
{
    class LoginValidation
    {
        public bool ValidLogin(string user, string passEncrypted)
        {
            LoginHandler handler = new LoginHandler();
            Logins l = handler.Load();
            Dictionary<string, string> logins = l.GetLogins();

            foreach(KeyValuePair<string, string> entry in logins)
            {
                if(entry.Key == user && entry.Value == passEncrypted)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
