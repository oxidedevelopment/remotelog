﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RemoteLogServer.Utils.Login
{
    [Serializable]
    class Logins
    {
        //This class is actively written to the current directionary.
        private Dictionary<string, string> logins = new Dictionary<string, string>();

        public Dictionary<string, string> GetLogins()
        {
            return logins;
        }

        public void SetLogins(Dictionary<string, string> logins)
        {
            this.logins = logins;
        }

    }
}
