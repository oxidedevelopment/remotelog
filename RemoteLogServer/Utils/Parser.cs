﻿using RemoteLog.Network;
using RemoteLogServer.Plugins;
using RemoteLogServer.Utils.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace RemoteLogServer.Utils
{
    class Parser
    {
        public void Parse(string data, TcpClient client)
        {
            bool validated = false;

            string[] args = data.Split(' ');
            foreach(string arg in args)
            {
                //First see if the command is a validate command.
                if(arg.Equals("validate"))
                {
                    //Login validation
                    LoginValidation validator = new LoginValidation();
                    if (!validator.ValidLogin(args[0].Split(':')[0], args[0].Split(':')[1]))
                    {
                        //Invalid login
                        Messenger.Send("invalid", client);
                        Console.WriteLine("Invalid login! closing the connection.");
                        client.Close(); //BAI
                    }
                    else
                    {
                        validated = true;
                    }

                } // arg equals validate

                //If we arent validated still.
                if (!validated)
                {
                    //Login validation
                    LoginValidation validator = new LoginValidation();
                    if (!validator.ValidLogin(args[0].Split(':')[0], args[0].Split(':')[1]))
                    {
                        //Invalid login
                        Messenger.Send("invalid", client);
                        Console.WriteLine("Invalid login! closing the connection.");
                        client.Close(); //BAI
                    }
                }
 
                //Pass on the command to plugins
                PluginManager.CallPlugins(data, client);
            }
        }
    }
}
