![coollogo_com-4540179.png](https://bitbucket.org/repo/jG94eo/images/3212369911-coollogo_com-4540179.png)


# README #

RemoteLog, beta.

### What is this repository for? ###

* Rapid Network prototyping
* Easy to use, credential required server.

### How do I get set up? ###

* Soon.

### Contribution guidelines ###

* Keep code neat and tidy.
* Some documentation, and a decent commit message.
* No major changes in code.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact